# k8s-pgbouncer

This role is pretty simple, you just need to list your connections in the ```vars/main.yml``` file.

```
pgpool:
  - name:
    dbname:
    host:
    user:
    password:
  - poolname:
    dbname:
    host:
    user:
    password:
```
